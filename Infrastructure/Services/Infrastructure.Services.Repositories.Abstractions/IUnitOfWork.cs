﻿using Infrastructure.Services.Repositories.Abstractions.Repositories;

namespace Infrastructure.Services.Repositories.Abstractions
{
    public interface IUnitOfWork
    {
        public IUserRepository Users { get; }

        public IMessageRepository Messages { get; }

        public IMessageFileRepository MessageFiles { get; }
    }
}
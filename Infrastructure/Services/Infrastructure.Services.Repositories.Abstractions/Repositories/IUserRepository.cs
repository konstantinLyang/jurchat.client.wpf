﻿using Domain.Entities;
using Infrastructure.Services.Repositories.Abstractions.Base;

namespace Infrastructure.Services.Repositories.Abstractions.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
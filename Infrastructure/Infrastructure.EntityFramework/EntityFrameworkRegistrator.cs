﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.EntityFramework
{
    public static class EntityFrameworkRegistrator
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(optionsBuilder =>
                        optionsBuilder.UseSqlite("DataSource=clientCash.db"))
                    .AddScoped<DbContext, ApplicationContext>();
            return services;
        }
    }
}
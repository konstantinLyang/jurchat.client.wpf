﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) {
            this.Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; } = null!;
        
        public DbSet<Message> Messages { get; set; } = null!;
        
        public DbSet<MessageFile> MessageFiles { get; set; } = null!;
    }
}
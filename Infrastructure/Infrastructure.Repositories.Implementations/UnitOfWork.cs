﻿using Infrastructure.Services.Repositories.Abstractions;
using Infrastructure.Services.Repositories.Abstractions.Repositories;

namespace Infrastructure.Repositories.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IUserRepository users, IMessageRepository messages, IMessageFileRepository messageFiles)
        {
            Users = users;

            Messages = messages;
            
            MessageFiles = messageFiles;
        }

        public IUserRepository Users { get; }

        public IMessageRepository Messages { get; }

        public IMessageFileRepository MessageFiles { get; }
    }
}
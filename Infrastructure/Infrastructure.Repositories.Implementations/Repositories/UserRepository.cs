﻿using Domain.Entities;
using Infrastructure.Repositories.Implementations.Base;
using Infrastructure.Services.Repositories.Abstractions.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Implementations.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext dbContext) : base(dbContext) { }
    }
}
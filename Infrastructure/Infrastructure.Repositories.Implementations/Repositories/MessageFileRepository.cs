﻿using Domain.Entities;
using Infrastructure.Repositories.Implementations.Base;
using Infrastructure.Services.Repositories.Abstractions.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Implementations.Repositories
{
    internal class MessageFileRepository : Repository<MessageFile>, IMessageFileRepository
    {
        public MessageFileRepository(DbContext dbContext) : base(dbContext) { }
    }
}
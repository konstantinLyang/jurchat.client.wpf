﻿using Domain.Entities;
using Infrastructure.Repositories.Implementations.Base;
using Infrastructure.Services.Repositories.Abstractions.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Implementations.Repositories
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        public MessageRepository(DbContext dbContext) : base(dbContext) { }
    }
}
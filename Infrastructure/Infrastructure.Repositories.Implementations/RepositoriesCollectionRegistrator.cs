﻿using Infrastructure.Repositories.Implementations.Repositories;
using Infrastructure.Services.Repositories.Abstractions;
using Infrastructure.Services.Repositories.Abstractions.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Repositories.Implementations
{
    public static class RepositoriesCollectionRegistrator
    {
        public static IServiceCollection RegisterRepositories(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<IMessageRepository, MessageRepository>();

            services.AddTransient<IMessageFileRepository, MessageFileRepository>();

            return services;
        }
    }
}
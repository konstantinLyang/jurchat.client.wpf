﻿namespace Domain.Entities.Base
{
    interface IEntity<TId>
    {
        TId Id { get; set; }
    }
}
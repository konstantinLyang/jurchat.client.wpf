﻿using Domain.Entities.Base;

namespace Domain.Entities
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }

        public string NickName { get; set; } = null!;

        public string FirstName { get; set; } = null!;

        public string LastName { get; set; } = null!;

        public string? FatherName { get; set; } = null;

        public string Telephone { get; set; } = null!;

        public string? Email { get; set; } = null;

        public string? Photo { get; set; } = null;

        public DateTime CreateDateTime { get; set; }

        public DateTime LastVisitDateTime { get; set; }
    }
}
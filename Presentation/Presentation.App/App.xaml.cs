﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Presentation.App.Services;
using System.Windows;

namespace Presentation.App
{
    public partial class App : Application
    {
        public static Window ActiveWindow => Current.Windows.Cast<Window>().FirstOrDefault(w => w.IsActive);

        public static Window FocusedWindow => Current.Windows.Cast<Window>().FirstOrDefault(w => w.IsFocused);

        public static bool IsDesignMode { get; private set; } = true;

        private static IHost _host;

        public static IHost Host => _host ??= Program.CreateHostBuilder(Environment.GetCommandLineArgs()).Build();

        protected override async void OnStartup(StartupEventArgs e)
        {
            var host = Host;

            base.OnStartup(e);

            await host.StartAsync();

            if (System.Diagnostics.Debugger.IsAttached)
            {
                //string[] args = e.Args;
                Host.Services.GetRequiredService<IUserDialogService>().StartUp();
            }
            else
            {
                //string[] args = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;
                Host.Services.GetRequiredService<IUserDialogService>().StartUp();
            }
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            using var host = Host;

            await host.StopAsync();

            base.OnExit(e);
        }

        public static void ConfigureService(HostBuilderContext host, IServiceCollection services) => services.AddServices();
    }
}
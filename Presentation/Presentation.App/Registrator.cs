﻿using Microsoft.Extensions.DependencyInjection;
using Presentation.App.Services;
using Presentation.App.Services.Implementations;
using Presentation.App.ViewModels.Pages;
using Presentation.App.ViewModels.Windows;
using Presentation.App.Views.Pages;
using Presentation.App.Views.Windows;
using Infrastructure.Repositories.Implementations;
using Infrastructure.EntityFramework;

namespace Presentation.App
{
    /// <summary>
    /// Регистрирует все сервисы приложения
    /// </summary>
    public static class Registrator
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services.InstallServices()
                .AddView()
                .RegisterRepositories()
                .ConfigureContext();
        }

        private static IServiceCollection AddView(this IServiceCollection serviceCollection)
        {
            return serviceCollection.AddSingleton<MainWindowViewModel>()
                .AddTransient(s => new MainWindow { DataContext = s.GetRequiredService<MainWindowViewModel>() })
                .AddSingleton<LoginPageViewModel>()
                .AddTransient(s => new LoginPage { DataContext = s.GetRequiredService<LoginPageViewModel>() })
                .AddSingleton<RegisterPageViewModel>()
                .AddTransient(s => new RegisterPage { DataContext = s.GetRequiredService<RegisterPageViewModel>() })
                .AddSingleton<MainPageViewModel>()
                .AddTransient(s => new MainPage { DataContext = s.GetRequiredService<MainPageViewModel>() })
                .AddSingleton<SettingsPageViewModel>()
                .AddTransient(s => new SettingsPage { DataContext = s.GetRequiredService<SettingsPageViewModel>() });
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection.AddSingleton<IUserDialogService, UserDialogService>()
                .AddSingleton<IApplicationSettingsService, ApplicationSettingsService>()
                .AddTransient<NotifyService>();
        }
    }
}
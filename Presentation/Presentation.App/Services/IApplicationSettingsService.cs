﻿using Presentation.App.Models.Configuration;

namespace Presentation.App.Services
{
    interface IApplicationSettingsService
    {
        public Settings GetSettings();

        public void SetSettings(Settings entity);
    }
}
﻿namespace Presentation.App.Services
{
    internal interface INotifyService
    {
        public void ShowMessage(string message);
    }
}

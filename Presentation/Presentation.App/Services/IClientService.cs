﻿using Domain.Entities;

namespace Presentation.App.Services
{
    internal interface IClientService
    {
        bool UserRegister(User user);

        bool UserLogin(string login, string password);

        bool SendMessage(Message message, User user);
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Presentation.App.ViewModels.Windows;
using Presentation.App.Views.Pages;
using Presentation.App.Views.Windows;

namespace Presentation.App.Services.Implementations
{
    internal class UserDialogService : IUserDialogService
    {
        public UserDialogService(IServiceProvider services)
        {
            _services = services;
        }

        private readonly IServiceProvider _services;

        private MainWindow _mainWindow;

        private MainWindowViewModel _mainWindowViewModel;

        private MainPage _mainPage;

        private LoginPage _loginPage;

        private RegisterPage _registerPage;

        private SettingsPage _settingsPage;

        public void StartUp()
        {
            ShowMainWindow();
        }

        public void ShowMainWindow()
        {
            //TODO: check to signin

            _mainWindow ??= _services.GetRequiredService<MainWindow>();

            _mainWindowViewModel ??= _services.GetRequiredService<MainWindowViewModel>();

            ShowLoginPage();

            _mainWindow.Show();
        }

        public void ShowLoginPage()
        {
            _loginPage ??= _services.GetRequiredService<LoginPage>();

            _mainWindowViewModel.Page = _loginPage;
        }

        public void ShowMainPage()
        {
            _mainPage ??= _services.GetRequiredService<MainPage>();

            _mainWindowViewModel.Page = _mainPage;
        }

        public void ShowRegisterPage()
        {
            _registerPage ??= _services.GetRequiredService<RegisterPage>();

            _mainWindowViewModel.Page = _registerPage;
        }

        public void ShowSettingsPage()
        {
            _mainWindowViewModel.SettingsPageVisibility = System.Windows.Visibility.Visible;
        }
    }
}
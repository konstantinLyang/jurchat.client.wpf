﻿using Presentation.App.Models.Configuration;

namespace Presentation.App.Services.Implementations
{
    class ApplicationSettingsService : IApplicationSettingsService
    {
        public Settings GetSettings()
        {
            return new Settings { };
        }

        public void SetSettings(Settings entity)
        {
        }
    }
}
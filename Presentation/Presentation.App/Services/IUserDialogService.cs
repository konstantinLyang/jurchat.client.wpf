﻿namespace Presentation.App.Services
{
    internal interface IUserDialogService
    {
        public void StartUp();

        public void ShowMainWindow();

        public void ShowMainPage();

        public void ShowLoginPage();

        public void ShowRegisterPage();

        public void ShowSettingsPage();
    }
}
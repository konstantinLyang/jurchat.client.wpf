﻿using Microsoft.Extensions.DependencyInjection;
using Presentation.App.Commands;
using Presentation.App.ViewModels.Base;
using Presentation.App.Views.Pages;
using System.Windows;
using System.Windows.Controls;

namespace Presentation.App.ViewModels.Windows
{
    internal class MainWindowViewModel : BaseViewModel
    {
        public MainWindowViewModel() { }
        public MainWindowViewModel(IServiceProvider serviceProvider) : base()
        {
            _serviceProvider = serviceProvider;

            SettingsPage = serviceProvider.GetRequiredService<SettingsPage>();
        }

        private readonly IServiceProvider _serviceProvider;

        public Page? Page { get; set; }
        
        public Page? SettingsPage { get; set; }
        
        public Visibility SettingsPageVisibility { get; set; } = Visibility.Collapsed;

        #region Commands

        public LambdaCommand CloseSettingsPage => new(() => { SettingsPageVisibility = Visibility.Collapsed; });

        #endregion
    }
}
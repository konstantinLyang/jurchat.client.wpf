﻿using AsyncAwaitBestPractices.MVVM;
using Presentation.App.Commands;
using Presentation.App.Services;
using Presentation.App.ViewModels.Base;

namespace Presentation.App.ViewModels.Pages
{
    internal class LoginPageViewModel : BaseViewModel
    {
        public LoginPageViewModel() { }
        public LoginPageViewModel(IUserDialogService userDialogService) : base()
        {
            _userDialogService = userDialogService;
        }

        private readonly IUserDialogService _userDialogService;

        #region Fields

        public string Login { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;

        public string Info { get; set; } = string.Empty;

        public bool RememberUser { get; set; } = true;

        #endregion

        #region Commands

        public AsyncCommand AuthorizationCommand => new(OnAuthorization, CanAuthorizationCommand);
        private async Task OnAuthorization()
        {
            await Task.Run(() =>
            {
                
            });

            _userDialogService.ShowMainPage();
        }
        private bool CanAuthorizationCommand(object? p) => !string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(Password);

        public LambdaCommand OpenRegisterWindowCommand => new(() => { _userDialogService.ShowRegisterPage(); });

        public LambdaCommand ShowSettingsPageCommand => new(() => { _userDialogService.ShowSettingsPage(); });

        #endregion
    }
}
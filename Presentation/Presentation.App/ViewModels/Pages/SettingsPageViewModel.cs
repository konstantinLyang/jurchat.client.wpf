﻿using Presentation.App.Commands;
using Presentation.App.Models.Configuration;
using Presentation.App.Services;
using Presentation.App.ViewModels.Base;

namespace Presentation.App.ViewModels.Pages
{
    internal class SettingsPageViewModel : BaseViewModel
    {
        public SettingsPageViewModel(IApplicationSettingsService applicationSettingsService)
        {
            _applicationSettingsServices = applicationSettingsService;

            AppSettings = applicationSettingsService.GetSettings();
        }

        private readonly IApplicationSettingsService _applicationSettingsServices;

        public Settings AppSettings { get; set; }

        #region Commands

        public LambdaCommand ChangeSettingsCommand => new(OnChangingSettings);
        private void OnChangingSettings()
        {
            _applicationSettingsServices.SetSettings(AppSettings);
        }

        public LambdaCommand AboutApplicationCommand => new(OnAboutApplication);
        private void OnAboutApplication()
        {

        }

        #endregion
    }
}
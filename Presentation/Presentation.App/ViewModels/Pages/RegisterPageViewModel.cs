﻿using Domain.Entities;
using Infrastructure.Services.Repositories.Abstractions;
using Presentation.App.Commands;
using Presentation.App.ViewModels.Base;

namespace Presentation.App.ViewModels.Pages
{
    internal class RegisterPageViewModel : BaseViewModel
    {
        public RegisterPageViewModel() { }
        public RegisterPageViewModel(IUnitOfWork unitOfWork) : base()
        {
            _unitOfWork = unitOfWork;
        }

        private readonly IUnitOfWork _unitOfWork;

        public User User { get; set; } = new User();

        public string Password { get; set; } = string.Empty;
        public string SecondPassword { get; set; } = string.Empty;

        public LambdaCommand CreateUserCommand => new(OnCreatingUser, CanCreateUser);
        private void OnCreatingUser()
        {
            _unitOfWork.Users.Add(User);
            _unitOfWork.Users.SaveChanges();
        }
        private bool CanCreateUser() => !string.IsNullOrEmpty(User.FirstName)
            && !string.IsNullOrEmpty(User.LastName) && !string.IsNullOrEmpty(User.Telephone)
            && !string.IsNullOrEmpty(User.NickName) &&  Password == SecondPassword;
    }
}
﻿using Presentation.App.Commands;
using Presentation.App.ViewModels.Base;

namespace Presentation.App.ViewModels.Pages
{
    internal class MainPageViewModel : BaseViewModel
    {
        public MainPageViewModel() { }

        public string MessageText { get; set; } = string.Empty;
        public string SearchContactText { get; set; } = string.Empty;

        public LambdaCommand SendMessageCommand => new(OnSendingMessage, CanSendMessage);
        private void OnSendingMessage()
        {

        }
        private bool CanSendMessage() => !string.IsNullOrEmpty(MessageText);

        public LambdaCommand SendFileCommand => new(OnSendingFile, CanSendFile);
        private void OnSendingFile()
        {

        }
        private bool CanSendFile() => true;
    }
}
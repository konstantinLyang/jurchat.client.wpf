﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Presentation.App
{
    public static class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            var app = new App();
            app.InitializeComponent();
            app.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseContentRoot(Environment.CurrentDirectory)
                .ConfigureAppConfiguration((host, cfg) => cfg
                    .SetBasePath(Environment.CurrentDirectory)
                    .AddJsonFile("appsettings.json", true, true))
                    .ConfigureServices(App.ConfigureService);
    }
}